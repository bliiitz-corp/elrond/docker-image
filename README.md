# Elrond Node Docker Image

This docker image contain Elrond Node software 

Source code can be found [here](https://github.com/elrondnetwork/elrond-go)

The binary is stored on `/elrond/node` path and config is stored on `/elrond/node/config` path inside the container

`termui` is build too insite on `elrond-termui` witch can allow us make a remote exec

Exemple for kubernetes:

`kubectl exec -it <pod_name> /elrond/termui`

Exemple for local docker:

`docker exec -it <container_name> /elrond/termui`
