FROM ubuntu:18.04 as builder

ARG VERSION=v1.0.101
ARG CONFIG_VERSION=BoNwards-w36
ARG APPVERSION
ENV DEBIAN_FRONTEND noninteractive
ENV GO111MODULE on
ENV GOROOT /usr/local/go
ENV GOPATH /go
ENV PATH /go/bin:/usr/local/go/bin:$PATH

RUN apt update -qq && apt upgrade -y && \
    apt install build-essential -y && \
    apt install git rsync curl zip unzip jq gcc wget -qq -y && \
    wget https://dl.google.com/go/go1.13.linux-amd64.tar.gz && \
    tar -xvf go1.13.linux-amd64.tar.gz && \
    mv go /usr/local && \
    mkdir -p /go/src/github.com/ElrondNetwork

RUN cd /go/src/github.com/ElrondNetwork && \
    git clone https://github.com/elrondnetwork/elrond-go.git && \
    cd ./elrond-go && \
    git checkout $VERSION && \
    rm go.sum && \
    go mod vendor && \
    ARWEN_VERSION=$(cat go.mod | grep arwen-wasm-vm | sed 's/.* //') && \
    cd /go/src/github.com/ElrondNetwork/elrond-go && go get github.com/ElrondNetwork/arwen-wasm-vm/cmd/arwen@$ARWEN_VERSION && go build -o ./arwen github.com/ElrondNetwork/arwen-wasm-vm/cmd/arwen && \
    cp /go/src/github.com/ElrondNetwork/elrond-go/arwen /go/src/github.com/ElrondNetwork/elrond-go/cmd/node/

RUN mkdir -p /elrond && \
    git clone https://github.com/ElrondNetwork/elrond-config-mainnet /elrond/config && \
    cd /elrond/config && \
    git checkout $CONFIG_VERSION
    


WORKDIR /go/src/github.com/ElrondNetwork/elrond-go/cmd/node
RUN go build -i -v -ldflags="-X main.appVersion=${APPVERSION}"

WORKDIR /go/src/github.com/ElrondNetwork/elrond-go/cmd/termui
RUN go build

WORKDIR /go/src/github.com/ElrondNetwork/elrond-go/cmd/keygenerator
RUN go build

RUN  cp /go/src/github.com/ElrondNetwork/elrond-go/cmd/node/node /elrond && \ 
     cp /go/src/github.com/ElrondNetwork/elrond-go/cmd/node/arwen /elrond && \ 
     cp /go/src/github.com/ElrondNetwork/elrond-go/cmd/keygenerator/keygenerator /elrond && \ 
     cp /go/src/github.com/ElrondNetwork/elrond-go/cmd/termui/termui  /elrond

WORKDIR /elrond

ENTRYPOINT ["/elrond/node"]
CMD ["-use-log-view", "-rest-api-interface", "0.0.0.0:8080"]
EXPOSE 8080





